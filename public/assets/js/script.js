//Loops are structures that allow us to execute code repeatedly
/*
	There are three types of loops:
	1. While loop
	2. Do-While loop
	3. For loop
*/

//While loop - executes as long as the condition is satisfied
/*
while(condition to check){
	//code here
	//increment or decrement
}
*/

let count = 1; //initial statement
while (count <= 10){ //condition for the loop to work
	console.log("The current count is: " + count); //do anything inside the loop
	count++; //increment -> makes sure that the value of count changes and reaches 10
	//Q: what if count++ is not included?
}

//Do while loop - executes as long as the condition is satisfied and functions almost like a while loop except it make sure that the loop runs at least once.
let count2 = 10;
do {
	console.log(count2);
	count2--;
} while (count2 >= 0);

//This is how a do-while runs at least once:
let count3 = 1;
do {
	console.log("hello world");
	count3++;
} while(count3 <= 0);

//The main difference of a while and do-while can be summarized into this:
/*
while loop checks first before executing (check before gawa)
do-while loop executes first before checking. (gawa before check)
*/

//For loop is a loop that has 3 parts, the initial value, the condition and the iteration(increment/decrement)
/*
for(initial value; condition; increment/decrement){
	//code here
}
*/

for(let i = 0; i < 5; i++){
	console.log("The current count is " + i);
	console.log("Current count * 2 is " + (i*2));
}

//Mini Exercise:
/* 
Use any loop (while, do while, for) to display all the numbers from 1 and 10 and say if it is odd or even

Output:
1 -> odd
2 -> even
3 -> odd
...
10 -> even
*/

//Using for loop
for(let count = 1; count <= 10; count++){
	if(count % 2 == 0){
		console.log(count + " is even");
	} else {
		console.log(count + " is odd");
	}
}

//Continue and break
//Continue is a keyword that allows the loop to skip the current count and proceed to the next
//Break automatically stops the loop
for(let count = 1; count <= 10; count++){
	if (count == 5){
		continue;
	}
	console.log(count);
}

for(let count = 1; count <= 10; count++){
	if(count == 5){
		break;
	}
	console.log(count);
}

//Difference between ++a and a++; (preincrement vs postincrement)
let a = 1;
let b = 1;
a++;
++b;
console.log(a);
console.log(b);


//However the pre increment and post increment differs if you use it with the assignment operator
let c = 1;
let d = 1;
let e = c++; //e will take the original value of c before c is added to
let f = ++d; //f will take the value of d AFTER d is added to

console.log(e);
console.log(f);
//Loops are structures that allow us to execute code repeatedly
/*
	There are three types of loops:
	1. While loop
	2. Do-While loop
	3. For loop
*/

//While loop - executes as long as the condition is satisfied
/*
while(condition to check){
	//code here
	//increment or decrement
}
*/

let count = 1; //initial statement
while (count <= 10){ //condition for the loop to work
	console.log("The current count is: " + count); //do anything inside the loop
	count++; //increment -> makes sure that the value of count changes and reaches 10
	//Q: what if count++ is not included?
}

//Do while loop - executes as long as the condition is satisfied and functions almost like a while loop except it make sure that the loop runs at least once.
let count2 = 10;
do {
	console.log(count2);
	count2--;
} while (count2 >= 0);

//This is how a do-while runs at least once:
let count3 = 1;
do {
	console.log("hello world");
	count3++;
} while(count3 <= 0);

//The main difference of a while and do-while can be summarized into this:
/*
while loop checks first before executing (check before gawa)
do-while loop executes first before checking. (gawa before check)
*/

//For loop is a loop that has 3 parts, the initial value, the condition and the iteration(increment/decrement)
/*
for(initial value; condition; increment/decrement){
	//code here
}
*/

for(let i = 0; i < 5; i++){
	console.log("The current count is " + i);
	console.log("Current count * 2 is " + (i*2));
}

//Mini Exercise:
/* 
Use any loop (while, do while, for) to display all the numbers from 1 and 10 and say if it is odd or even

Output:
1 -> odd
2 -> even
3 -> odd
...
10 -> even
*/

//Using for loop
for(let count = 1; count <= 10; count++){
	if(count % 2 == 0){
		console.log(count + " is even");
	} else {
		console.log(count + " is odd");
	}
}

//Continue and break
//Continue is a keyword that allows the loop to skip the current count and proceed to the next
//Break automatically stops the loop
for(let count = 1; count <= 10; count++){
	if (count == 5){
		continue;
	}
	console.log(count);
}

for(let count = 1; count <= 10; count++){
	if(count == 5){
		break;
	}
	console.log(count);
}

//Difference between ++a and a++; (preincrement vs postincrement)
let a = 1;
let b = 1;
a++;
++b;
console.log(a);
console.log(b);


//However the pre increment and post increment differs if you use it with the assignment operator
let c = 1;
let d = 1;
let e = c++; //e will take the original value of c before c is added to
let f = ++d; //f will take the value of d AFTER d is added to

console.log(e);
console.log(f);
